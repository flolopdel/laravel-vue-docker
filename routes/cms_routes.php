<?php

Route::resource('products', 'productController');
Route::group(['prefix' => 'product'], function () {

	Route::get('/index-vue', 'productController@indexVue');
});