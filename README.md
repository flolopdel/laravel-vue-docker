# CMS - LARAVEL
##### Cms created with http://labs.infyom.com/laravelgenerator/ and docker containers

Instalation with Docker:

1. clone project
2. cd /project_folder/docker
3. docker-compose up --build -d

When all the containers are running:

1. docker-compose exec php bash (connect to php container)
2. composer install
3. npm install
4. php artisan migrate:refresh --seed
5. php artisan key:generate
6. Go to http://localhost:8080/login in your browser

Frontend vue https://laravel.com/docs/5.5/frontend

UNIT TEST
1. Create a test in the Feature directory.
php artisan make:test UserTest
2. Create a test in the Unit directory.
php artisan make:test UserTest --unit
3. ./vendor/bin/phpunit